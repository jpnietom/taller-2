#TALLER
#Se tienen dos tuplas que almacenan la estatura y el peso de un conjunto 
#De 10 seres humanos
#Una tupla la estatura y la otra almacena el peso
#Crear un lista que almacene el IMC de cada uno de los seres humanos
#Emplear solo listas, tuplas, ciclos for...in o while
#Debe entregar una lista llamada IMC con los 10 datos

Estatura=(1.75,1.60,1.55,1.80,2.0,1.77,1.70,1.82,1.69,1.84)
Peso=(56,77,34,45,89,110,54,70,67,82)
IMC=[0,1,2,3,4,5,6,7,8,9]

for x in IMC:
    Calculo=Peso[x]/Estatura[x]
    IMC[x]=Calculo
print(IMC)    
